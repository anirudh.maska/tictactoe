Rails.application.routes.draw do
  get "/game/index", to:"games#index"
  get "/game/join", to:"games#join"
  post "/game/create", to:"games#create"
  post "/game/join-player", to: "games#join_player"
  get "/game/debug", to:"games#game"
  match '*a', to: 'application#render_404', via: [:get, :post, :put, :delete]
end
