require 'hashie'

class ClientEvent
  MONITORED_CHANNELS = [ '/meta/subscribe', '/meta/disconnect' ]

  def incoming(message, callback)
    return callback.call(message) unless MONITORED_CHANNELS.include? message['channel']
    faye_msg = Hashie::Mash.new(message)
    faye_action = faye_msg.channel.split('/').last
    if faye_action == "disconnect"
      faye_client.publish(connected_clients[faye_msg.clientId], {:message => "disconnected"})
    end
    get_client(faye_msg.clientId,faye_action,faye_msg.subscription)
    callback.call(message)
  end

  def connected_clients
    @connected_clients ||= { }
  end

  def push_client(client_id,channel)
    connected_clients[client_id] = channel
  end

  def pop_client(client_id)
    connected_clients.delete(client_id)
  end

  def get_client(client_id, action,channel)
    if action == 'subscribe'
      push_client(client_id,channel)
    elsif action == 'disconnect'
      pop_client(client_id)
    end
  end

  def faye_client
    @faye_client ||= Faye::Client.new(ENV["FAYE_END_POINT"])
  end

end