require 'securerandom'
class GamesController  < ApplicationController
	skip_before_action :verify_authenticity_token
	def create
		begin
			@channel_id = "/board/#{SecureRandom.uuid.gsub('-', '')}"
			logger.info "channel created #{@channel_id}"
			@user_id = ""
			logger.info "new boarded created #{params.inspect}"
			attributes ={
				channel_id: @channel_id,
				player1: {
					name: params['name'],
					email: params['email'],
					sex: params['sex']
				},
				active: true
			}
			@user_id = "#{params['name'].delete(' ')}#{rand(100...999)}"
			@player1_name = params['name']
			board = Game.new(attributes)
			if board.save!
				logger.info "created successfully"
			end
		rescue Exception => e
			logger.error "Something went wrong #{e.inspect} #{e.backtrace}"
		end
		logger.info "channel - #{@channel_id} user - #{@user_id}"
		render "/games/game.html.erb"
	end
	def join_player
		begin
			logger.info "joing  the board params #{params.inspect}"
			@channel_id = params['channel']
			@user_id = ""
			@player1_name = ""
			attributes = {
				player2:{
					name: params['name'],
					email: params['email'],
					sex: params['sex']
				}
			}
			board = Game.where("channel_id = '#{@channel_id}'").last
			if(board.player2.nil?)
				@player1_name = board.player1['name']
				@player2_name = params['name'] 
				board.update_attributes(attributes)
				if board.save!
					logger.info "board updated successfully"
				end
				@user_id = "#{params['name'].delete(' ')}#{rand(100...999)}"
			else
				@reason = "alreay 2 people are playing on the board create a new one"
				render "/games/index.html.erb"
				return
			end
		rescue Exception => e
			logger.error "Something went wrong #{e.inspect} #{e.backtrace}"
		end
		render "/games/game.html.erb"
	end
end
