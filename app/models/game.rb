require 'mongoid'
require 'securerandom'
class Game
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in collection: "tictactoe"
    field :channel_id, type: String, default: "/board/#{SecureRandom.uuid.gsub('-', '')}"
    field :active, type: Boolean
    field :player1, type: Hash
    field :player2, type: Hash
    field :created_at, type: DateTime, default: ->{ Time.now }
    field :updated_at, type: DateTime, default: ->{ Time.now }
    field :winner, type: String
    index ({board_id: 1, channel_id: 1})
    
end